<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wp');

/** Имя пользователя MySQL */
define('DB_USER', 'wp-db');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1q2w3e');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');
define('FS_METHOD','direct');
define( 'WP_DEBUG', true );
/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IP?4C.BnRKE*@Cfz+(9BM;hSSfYgBQK3>>`7FeYEB4@lSs14DgE0Gq)TS{7-6@QD');
define('SECURE_AUTH_KEY',  'lmI#EB<=8Z45Uv8ZY[2S9cE]^<>o^[I|ks*%c}~,+5WN6.18neuri4}#A&tNpS/F');
define('LOGGED_IN_KEY',    '4O-YbYof5C[%,fv]5tS{UFgi1o!Ng2i<5c}W?2L`aK{y)8N 39E<P$[`)XOoRl]s');
define('NONCE_KEY',        'u3V9Z]Y:^sYJ9/K9,#^&jYMk9Du`AHnBs@_]Kp0~80Pfh~%$x)MTY-:(&ecMVHAY');
define('AUTH_SALT',        'L8F@DPTDyLiiPGR5?-BDuqXaSvdDv<4zx}+a}Gg#S|*$CmyMbgQ+wzy~dN]`rj2-');
define('SECURE_AUTH_SALT', 'P@ <}5?A;[CwyP,E5a/UBcX[@>c]Fc(L!#k=PI {9,26KW:i[#gi|{|JU%DB~DI4');
define('LOGGED_IN_SALT',   'G.5mVKpdV)3eD#~BcEgg4VVv&PpMY%8iIHM8YF;!P<@ecLe28E+|WTjTFE_WW4ul');
define('NONCE_SALT',       '0u ;@c<<&j,YoHRs`/7H*s+;{+4HTTt/#r#ZQ&1i{l_/X^`0qkYQ. t|@a7*;;<a');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
