<div class="footer">

    <div class="container">
        <div class="col-md-3 fcent">
            <div class="footer-logo logo" ><a href="<?php echo get_home_url(); ?>"><?php


                    global $redux_demo;
                    echo $redux_demo['footer-text'];
                    ?></a></div>
            <div class="social-icon">

                <?php if ($redux_demo['social-fb'] !==''){?>
                    <a href="<?php $redux_demo['social-fb']?>"><i class="fa fa fa-facebook"></i> </a>
                <?php }?>
                <?php if ($redux_demo['social-tw'] !==''){?>
                    <a href="<?php $redux_demo['social-tw']?>"><i class="fa fa fa-twitter"></i> </a>
                <?php }?>
                <?php if ($redux_demo['social-be'] !==''){?>
                    <a href="<?php $redux_demo['social-be']?>"><i class="fa fa-behance"></i> </a>
                <?php }?>
                <?php if ($redux_demo['social-dr'] !==''){?>
                    <a href="<?php $redux_demo['social-dr']?>"><i class="fa fa-dribbble"></i> </a>
                <?php }?>
                <?php if ($redux_demo['socail-in'] !==''){?>
                    <a href="<?php $redux_demo['socail-in']?>"><i class="fa fa-linkedin"></i> </a>
                <?php }?>

            </div>
        </div>
        <div class="col-md-5">
            <div class="menu-footer ">
                <nav class="list-inline">

                            <?php
                            $params = array(
                                'container'=> false, // Без div обертки
                                'echo'=> false, // Чтобы можно было его предварительно вернуть
                                'items_wrap'=> '%3$s', // Разделитель элементов
                                'depth'=> 0, // Глубина вложенности
                                'menu' => 'footer',
                            );
                            // Чистим все теги, кроме ссылок
                            print strip_tags(wp_nav_menu( $params ), '<a>, <li>' );
                            ?>

                               </nav></div>
            <div class="subscire">Subscribe to news</div>
            <div class="subscribe_form">
                <form action="">
                    <div class="input-group sub_input">
<span class="inputmy"><input type="text" class="form-control pull-left inputmy"  placeholder="Email adres" type="email" style="width: 65%;"></span>
                        <button type="button" class="btn btn-default pull-right">Middle</button>
                    </div>   </form>
            </div>

        </div>
        <div class="col-md-4 fcent">
            <div class="contact-block">Contact</div>
            <div class="adres-contact"><?php echo $redux_demo['adress-textarea'];?></div>
            <div class="paymant">
                <?php
                if ( isset($redux_demo['opt-slides-content12']) && !empty($redux_demo['opt-slides-content12']) )
                    foreach( $redux_demo['opt-slides-content12'] as $key_slide):
                        ?>
                        <a href="<?php echo $key_slide['url']; ?>"> <img  src="<?php echo $key_slide['image']; ?>"  alt="Slide image"></a>
                    <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>