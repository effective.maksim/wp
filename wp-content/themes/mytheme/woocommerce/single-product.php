<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


		<?php while ( have_posts() ) : the_post(); ?>
	<div class="title-pruduct">
		<div class="cat-title">
            <?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
		</div>
		<div class="breadcrump">
            <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		</div>
	</div>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>
	<div class="col-md-12 mycol">
        <div class="container tabb">
		<ul class="nav nav-tabs tabbb">
			<li class=""><a data-toggle="tab" href="#Review">Review</a></li>
			<li><a data-toggle="tab" href="#Specification">Specification</a></li>
			<li><a data-toggle="tab" href="#Description">Description</a></li>

		</ul>
        </div>
        <div class="container tabb">
        <!--review-->
		<div class="tab-content">
            <?php comments_template(); ?>

		</div>

		<!--Specification-->
		<div id="Specification" class="tab-pane fade specif">
            <?php do_action( 'woocommerce_product_additional_information', $product ); ?>
		</div>

		<!--Description-->
		<div id="Description" class="tab-pane fade desc">
            <?php the_content(); ?>
		</div>
        </div>
	</div>
	<!-- content block -->
	<div class="container">
        <?php
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'paged' => 1,
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) :
            ?>
			<div class="my-posts">
                <?php $count = '0';?>
                <?php while ( $query->have_posts() ) : $query->the_post() ?>
					<div class="col-md-3">
						<div class="mypost">
							<div class="borderblock">
								<div class="post-img"><a href="<?php the_permalink(); ?>"> <?php if(has_post_thumbnail()){ the_post_thumbnail();}?></a></div>
								<div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

								<!-- if isset sale price -> change class -->
								<div class="price-color">
                                    <?php
                                    global $product;
                                    $sale_price=$product->get_sale_price();
                                    if ($sale_price !=='')
                                    {
                                        echo '<span class="old-price">';
                                    }
                                    ?>

                                    <?php
                                    echo $old_price=$product->get_regular_price() .' USD';
                                    ?>

                                    <?php
                                    if ($sale_price !=='')
                                    {
                                        echo '</span>';
                                    }
                                    ?>

                                    <?php
                                    if ($sale_price !=='')
                                    {
                                        echo ' ' .$sale_price .' USD';
                                    }
                                    ?>
								</div>
								<!-- count pruduct -->
                                <?php
                                $count++;
                                wp_reset_postdata();
                                ?>

							</div></div>
					</div>
					<!-- Show banner after 5 count -->
                    <?php  if ($count === 4){
                        break;
                    }?>
					<!-- END Show banner after 5 post -->
                <?php endwhile ?>
			</div>
        <?php endif ?>
	</div>
	<!-- END content block -->
	<div class="container">
		<div class="caruosel-bloc">
			<section class="slick-carousel11">
                <?php
                global $redux_demo;

                if ( isset($redux_demo['partner-slides-content']) && !empty($redux_demo['partner-slides-content']) )
                    foreach( $redux_demo['partner-slides-content'] as $key_slide):
                        ?>
						<div><a href="<?php echo $key_slide['url']; ?>"><img src="<?php echo $key_slide['image']; ?>" alt=""></a></div>


                    <?php endforeach; ?>
			</section>
		</div>

	</div>
		<?php endwhile; // end of the loop. ?>




<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
