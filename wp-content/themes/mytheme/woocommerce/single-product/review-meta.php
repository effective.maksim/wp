<?php
/**
 * The template to display the reviewers meta data (name, verified owner, review date)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review-meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $comment;
$verified = wc_review_is_from_verified_owner( $comment->comment_ID );

if ( '0' === $comment->comment_approved ) { ?>

	<p class="meta">
		<em class="woocommerce-review__awaiting-approval">
			<?php esc_html_e( 'Your review is awaiting approval', 'woocommerce' ); ?>
		</em>
	</p>

<?php } else { ?>

	<div class="flex_meta1">

		<div class="flex_1">
            <?php
            /**
             * The woocommerce_review_before hook
             *
             * @hooked woocommerce_review_display_gravatar - 10
             */
            do_action( 'woocommerce_review_before', $comment );
            ?>
            <span class="author_flex"><?php comment_author(); ?> </span>

           <div class="rating_meta"><?php
            $rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );

            if ( $rating && 'yes' === get_option( 'woocommerce_enable_review_rating' ) ) {
                echo wc_get_rating_html( $rating );
            }
            ?>
           </div>
</div>

        <div class="flex_2">
        <?php
		if ( 'yes' === get_option( 'woocommerce_review_rating_verification_label' ) && $verified ) {
			echo '<em class="woocommerce-review__verified verified">(' . esc_attr__( 'verified owner', 'woocommerce' ) . ')</em> ';
		}
		?>
            <div class="comp-time"><i class="fa fa-clock-o"></i> Date: <time class="woocommerce-review__published-date" datetime="<?php echo esc_attr( get_comment_date( '' ) ); ?>"><?php echo esc_html( get_comment_date( 'n F,Y' ) ); ?></time></div>
            <div class="mob-time"><time class="woocommerce-review__published-date" datetime="<?php echo esc_attr( get_comment_date( '' ) ); ?>"><?php echo esc_html( get_comment_date( ) ); ?></time></div>
        </div></div>

<?php
}
