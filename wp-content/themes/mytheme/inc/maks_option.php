<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "redux_demo";

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns      = array();

if ( is_dir( $sample_patterns_path ) ) {

    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
        $sample_patterns = array();

        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                $name              = explode( '.', $sample_patterns_file );
                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'             => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'         => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version'      => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type'            => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'       => true,
    // Show the sections below the admin menu item or not
    'menu_title'           => __( 'Maks Options', 'redux_demo' ),
    'page_title'           => __( 'Maks Options', 'redux_demo' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key'       => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography'     => false,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar'            => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon'       => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority'   => 50,
    // Choose an priority for the admin bar menu
    'global_variable'      => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode'             => true,
    // Show the time the page took to load, etc
    'update_notice'        => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer'           => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority'        => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'          => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'     => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon'            => '',
    // Specify a custom URL to an icon
    'last_tab'             => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon'            => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug'            => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults'        => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show'         => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark'         => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export'   => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'           => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database'             => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn'              => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints'                => array(
        'icon'          => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color'    => 'lightgray',
        'icon_size'     => 'normal',
        'tip_style'     => array(
            'color'   => 'red',
            'shadow'  => true,
            'rounded' => false,
            'style'   => '',
        ),
        'tip_position'  => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect'    => array(
            'show' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'mouseover',
            ),
            'hide' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'click mouseleave',
            ),
        ),
    )
);




Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */

Redux::setSection( $opt_name, array(
    'title'            => __( 'Header', 'redux_demo' ),
    'id'               => 'general-section',
    'desc'             => __( 'Header settings: ', 'redux_demo' ),
    'icon'  => 'el el-home-alt',
    'fields'           => array(
        array(
            'id'       => 'logo-text',
            'type'     => 'text',
            'title'    => __('Logo text', 'redux_demo'),
            'default'  => 'Test'
        ),
    ),

) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Homepage', 'redux_demo' ),
    'id'         => 'Homepage-settings',
    'desc'       => __( 'All settings: ', 'redux-demo' ),
    'icon'  => 'el el-screen',


) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Slides header', 'redux_demo' ),
    'id'         => 'additional-slides',
    'desc'       => __( 'Add slides on home page: ', 'redux-demo' ),
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'opt-slides',
            'type'        => 'slides',
            'title'       => __( 'Slides Options', 'redux-demo' ),
            'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'redux-demo' ),
            'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'redux-demo' ),
            'placeholder' => array(
                'title'       => __( 'This is a title', 'redux-demo' ),
                'description' => __( 'Description Here', 'redux-demo' ),
                'button'        => __( 'Give us a link!', 'redux-demo' ),
                'url'         => __( 'Give us a link!', 'redux-demo' ),
            ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Services', 'redux_demo' ),
    'id'               => 'Services-section',
    'desc'             => __( 'Services settings: ', 'redux_demo' ),
    'subsection' => true,
    'fields'           => array(
        array(
            'id'       => 'section-1-icon',
            'type'     => 'text',
            'title'    => __( 'Section-1', 'redux-demo' ),
            'subtitle' => __( 'Section-1 icon', 'redux-demo' ),
            'default'  => '<i class="fa fa-phone"></i>',
        ),
        array(
            'id'       => 'section-1',
            'type'     => 'text',
            'title'    => __( 'Section-1', 'redux-demo' ),
            'subtitle' => __( 'Section-1 Title', 'redux-demo' ),
            'default'  => 'Free Shiping',
        ),
        array(
            'id'       => 'section-1-sub',
            'type'     => 'text',
            'title'    => __( 'Section-1', 'redux-demo' ),
            'subtitle' => __( 'Section-1 subtitle', 'redux-demo' ),
            'default'  => 'Oders over 99$',
        ),
        array(
            'id'       => 'section-2-icon',
            'type'     => 'text',
            'title'    => __( 'Section-2', 'redux-demo' ),
            'subtitle' => __( 'Section-2 icon', 'redux-demo' ),
            'default'  => '<i class="fa fa-phone"></i>',
        ),
        array(
            'id'       => 'section-2',
            'type'     => 'text',
            'title'    => __( 'Section-2', 'redux-demo' ),
            'subtitle' => __( 'Section-2 Title', 'redux-demo' ),
            'default'  => '30 days return',
        ),
        array(
            'id'       => 'section-2-sub',
            'type'     => 'text',
            'title'    => __( 'Section-2', 'redux-demo' ),
            'subtitle' => __( 'Section-2 subtitle', 'redux-demo' ),
            'default'  => 'If goods have problem',
        ),
        array(
            'id'       => 'section-3-icon',
            'type'     => 'text',
            'title'    => __( 'Section-3', 'redux-demo' ),
            'subtitle' => __( 'Section-3 icon', 'redux-demo' ),
            'default'  => '<i class="fa fa-phone"></i>',
        ),
        array(
            'id'       => 'section-3',
            'type'     => 'text',
            'title'    => __( 'Section-3', 'redux-demo' ),
            'subtitle' => __( 'Section-3 Title', 'redux-demo' ),
            'default'  => 'Secure paymant',
        ),
        array(
            'id'       => 'section-3-sub',
            'type'     => 'text',
            'title'    => __( 'Section-3', 'redux-demo' ),
            'subtitle' => __( 'Section-3 subtitle', 'redux-demo' ),
            'default'  => '100% secure paymant',
        ),
        array(
            'id'       => 'section-4-icon',
            'type'     => 'text',
            'title'    => __( 'Section-4', 'redux-demo' ),
            'subtitle' => __( 'Section-4 icon', 'redux-demo' ),
            'default'  => '<i class="fa fa-phone"></i>',
        ),
        array(
            'id'       => 'section-4',
            'type'     => 'text',
            'title'    => __( 'Section-4', 'redux-demo' ),
            'subtitle' => __( 'Section-4 Title', 'redux-demo' ),
            'default'  => '24h support',
        ),
        array(
            'id'       => 'section-4-sub',
            'type'     => 'text',
            'title'    => __( 'Section-4', 'redux-demo' ),
            'subtitle' => __( 'Section-4 subtitle', 'redux-demo' ),
            'default'  => 'Dedicated Support',
        ),
    )

) );
Redux::setSection( $opt_name, array(
    'title'            => __( 'Content banner', 'redux_demo' ),
    'id'               => 'banner-section',
    'desc'             => __( 'Banner settings: ', 'redux_demo' ),
    'subsection' => true,
    'fields'           => array(
        array(
            'id'       => 'banner-switch',
            'type'     => 'switch',
            'title'    => __('On/Off', 'redux-framework-demo'),
            'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'banner-img',
            'type'     => 'text',
            'title'    => __('Banner img url', 'redux_demo'),
            'default'  => 'http://devit.tk/wp/wp-content/uploads/2018/09/Index-1.gif'
        ),
        array(
            'id'       => 'banner-text',
            'type'     => 'text',
            'title'    => __('Banner text', 'redux_demo'),
            'default'  => 'Banner text'
        ),
        array(
            'id'       => 'price-banner-text',
            'type'     => 'text',
            'title'    => __('Price text', 'redux_demo'),
            'default'  => '50'
        ),
        array(
            'id'       => 'catalog-banner-text',
            'type'     => 'text',
            'title'    => __('Price text', 'redux_demo'),
            'default'  => 'Lifestyle'
        ),
        array(
            'id'       => 'banner-link',
            'type'     => 'text',
            'title'    => __('Banner link', 'redux_demo'),
            'default'  => 'http://devit.tk/'
        ),
        array(
            'id'       => 'banner-img',
            'type'     => 'media',

            'url'      => true,
            'title'    => __( 'Banner img', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
            'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
    ),

) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Slides content', 'redux_demo' ),
    'id'         => 'additional-slides-content',
    'desc'       => __( 'Add slides on home page: ', 'redux-demo' ),
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'opt-slides-content',
            'type'        => 'slides',
            'title'       => __( 'Slides Options', 'redux-demo' ),
            'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'redux-demo' ),
            'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'redux-demo' ),
            'placeholder' => array(
                'title'       => __( 'This is a title', 'redux-demo' ),
                'description' => __( 'Description Here', 'redux-demo' ),
                'button'        => __( 'Give us a link!', 'redux-demo' ),
                'url'         => __( 'Give us a link!', 'redux-demo' ),
            ),

        ),

    )
) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'After content banner', 'redux_demo' ),
    'id'               => 'after-banner-section',
    'desc'             => __( 'After content banner: ', 'redux_demo' ),
    'subsection' => true,
    'fields'           => array(
        array(
            'id'       => 'after-banner-switch-1',
            'type'     => 'switch',
            'title'    => __('On/Off banner 1', 'redux-framework-demo'),
            'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'banner1-text-1',
            'type'     => 'text',
            'title'    => __('Banner category url', 'redux_demo'),
            'default'  => 'Suits'
        ),
        array(
            'id'       => 'banner1-text-2',
            'type'     => 'text',
            'title'    => __('Title banner 1', 'redux_demo'),
            'default'  => 'Slim Fit Prince of <br>Wales Check Wool'
        ),
        array(
            'id'       => 'banner1-text-3',
            'type'     => 'text',
            'title'    => __('banner 1 - Price, USD', 'redux_demo'),
            'default'  => '2,295.00'
        ),
        array(
            'id'       => 'banner1-text-4',
            'type'     => 'text',
            'title'    => __('Banner 1 link', 'redux_demo'),
            'default'  => 'http://devit.tk/'
        ),

        array(
            'id'       => 'banner1-img',
            'type'     => 'media',
            'width'    => '570',

            'url'      => true,
            'title'    => __( 'Banner 1 img', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
            'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
        array(
            'id'       => 'after-banner-switch-2',
            'type'     => 'switch',
            'title'    => __('On/Off banner 2', 'redux-framework-demo'),
            'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'banner2-text-1',
            'type'     => 'text',
            'title'    => __('Banner 2 category url', 'redux_demo'),
            'default'  => 'http://devit.tk/wp/'
        ),
        array(
            'id'       => 'banner2-text-2',
            'type'     => 'text',
            'title'    => __('Title banner 2', 'redux_demo'),
            'default'  => 'Slim Fit Prince of <br>Wales Check Wool'
        ),
        array(
            'id'       => 'banner2-text-3',
            'type'     => 'text',
            'title'    => __('banner 2 - Price, USD', 'redux_demo'),
            'default'  => '2,295.00'
        ),
        array(
            'id'       => 'banner2-link',
            'type'     => 'text',
            'title'    => __('Banner 2 link', 'redux_demo'),
            'default'  => 'http://devit.tk/'
        ),
        array(
            'id'       => 'banner2-text-4',
            'type'     => 'text',
            'title'    => __('Banner 2 link', 'redux_demo'),
            'default'  => 'http://devit.tk/'
        ),
        array(
            'id'       => 'banner2-img',
            'type'     => 'media',
            'width'    => '570',

            'url'      => true,
            'title'    => __( 'Banner 2 img', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
            'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
    ),

) );
Redux::setSection( $opt_name, array(
    'title'            => __( 'Banner after content left', 'redux_demo' ),
    'id'               => 'banner-section-left',
    'desc'             => __( 'Banner settings: ', 'redux_demo' ),
    'subsection' => true,
    'fields'           => array(
        array(
            'id'       => 'banner-switch-left',
            'type'     => 'switch',
            'title'    => __('On/Off', 'redux-framework-demo'),
            'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
            'default'  => true,
        ),
        array(
            'id'       => 'banner-left-img',
            'type'     => 'text',
            'title'    => __('Banner left url', 'redux_demo'),
            'default'  => 'http://devit.tk/wp/wp-content/uploads/2018/09/Index-1.gif'
        ),
        array(
            'id'       => 'banner-left-text',
            'type'     => 'text',
            'title'    => __('Banner text top', 'redux_demo'),
            'default'  => 'Number of day'
        ),
        array(
            'id'       => 'banner-left-count',
            'type'     => 'text',
            'title'    => __('Banner count', 'redux_demo'),
            'default'  => '197'
        ),
        array(
            'id'       => 'banner-left-content',
            'type'     => 'text',
            'title'    => __('Banner text content', 'redux_demo'),
            'default'  => 'Combo Sale Off <br>Up To 50%'
        ),
        array(
            'id'       => 'banner-left-link',
            'type'     => 'text',
            'title'    => __('Banner link', 'redux_demo'),
            'default'  => 'http://devit.tk/'
        ),
        array(
            'id'       => 'banner-left-img1',
            'type'     => 'media',

            'url'      => true,
            'title'    => __( 'Banner img', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
            'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
    ),

) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Slides partners', 'redux_demo' ),
    'id'         => 'additional-slides-content1',
    'desc'       => __( 'Add slides on page: ', 'redux-demo' ),
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'partner-slides-content',
            'type'        => 'slides',
            'title'       => __( 'Slides Options', 'redux-demo' ),
            'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'redux-demo' ),
            'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'redux-demo' ),
            'placeholder' => array(
                'title'       => __( 'This is a title', 'redux-demo' ),
                'url'         => __( 'Give us a link!', 'redux-demo' ),
            ),

        ),

    )
) );
Redux::setSection( $opt_name, array(
    'title'            => __( 'Fotter', 'redux_demo' ),
    'id'               => 'general-section1',
    'desc'             => __( 'Header settings: ', 'redux_demo' ),
    'icon'  => 'el el-home-alt',
    'fields'           => array(
        array(
            'id'       => 'footer-text',
            'type'     => 'text',
            'title'    => __('Logo footer text', 'redux_demo'),
            'default'  => 'Test'
        ),
        array(
            'id'=>'adress-textarea',
            'type' => 'textarea',
            'title' => __('Adress footer text', 'redux-framework-demo'),
            'subtitle' => __('Custom HTML Allowed (wp_kses)', 'redux-framework-demo'),
            'desc' => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
            'validate' => 'html_custom',
            'default' => 'San Francisco, California<br>400 Castro St, San Francisco, CA<br>(+1) 686-868-9999',
            'allowed_html' => array(
                'a' => array(
                    'href' => array(),
                    'title' => array()
                ),
                'br' => array(),
                'em' => array(),
                'strong' => array()
            )
        ),
    ),

) );

Redux::setSection( $opt_name, array(
    'title'            => __( 'Social icons', 'redux_demo' ),
    'id'               => 'social-section',
    'desc'             => __( 'Social url: ', 'redux_demo' ),
    'subsection' => true,
    'fields'           => array(

        array(
            'id'       => 'social-fb',
            'type'     => 'text',
            'title'    => __( 'Facebook url', 'redux-demo' ),
            'subtitle' => __( 'Facebook url', 'redux-demo' ),
            'default'  => 'http://facebook.com',
        ),
        array(
            'id'       => 'social-tw',
            'type'     => 'text',
            'title'    => __( 'Twitter url', 'redux-demo' ),
            'subtitle' => __( 'Twitter url', 'redux-demo' ),
            'default'  => 'http://Twitter.com',
        ),
        array(
            'id'       => 'social-be',
            'type'     => 'text',
            'title'    => __( 'Be url', 'redux-demo' ),
            'subtitle' => __( 'Be url', 'redux-demo' ),
            'default'  => 'https://www.behance.net/',
        ),
        array(
            'id'       => 'social-dr',
            'type'     => 'text',
            'title'    => __( 'dribbble url', 'redux-demo' ),
            'subtitle' => __( 'dribbble url', 'redux-demo' ),
            'default'  => 'https://dribbble.com/"></i>',
        ),
        array(
            'id'       => 'socail-in',
            'type'     => 'text',
            'title'    => __( 'linkedin url', 'redux-demo' ),
            'subtitle' => __( 'linkedin url', 'redux-demo' ),
            'default'  => 'https://www.linkedin.com/',
        ),

    )

) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Partners icon', 'redux_demo' ),
    'id'         => 'additional-slides-content11',
    'desc'       => __( 'Add Partners icon: ', 'redux-demo' ),
    'subsection' => true,
    'fields'     => array(
        array(
            'id'          => 'opt-slides-content12',
            'type'        => 'slides',
            'title'       => __( 'Partners icon Options', 'redux-demo' ),
            'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'redux-demo' ),
            'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'redux-demo' ),
            'placeholder' => array(
                'title'       => __( 'This is a title', 'redux-demo' ),
                'description' => __( 'Description Here', 'redux-demo' ),
                'button'        => __( 'Give us a link!', 'redux-demo' ),
                'url'         => __( 'Give us a link!', 'redux-demo' ),
            ),

        ),

    )
) );