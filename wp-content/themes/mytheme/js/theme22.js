(function ($, root, undefined) {
    $(function () {
        'use strict';

        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        console.log('asasd');

        $('.slider_content').slick({
            dots: true,
            infinite: true,
            arrov:true,
            nextArrow: '<i class="fa fa-arrow-right next"></i>',
            prevArrow: '<i class="fa fa-arrow-left prev"></i>',
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        $('.three1').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            mobileFirst: true,
            prevArrow: $(".pp2"),
            nextArrow: $(".nn2"),
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow: 2
                }
            }]

        });


        var ajaxurl = "http://localhost/wp/wp-admin/admin-ajax.php";
        var page = 1;
        var offset = 0;
        jQuery(function($) {
            $('body').on('click', '.loadmore', function() {
                var data = {
                    'action': 'load_posts_by_ajax',
                    'page': page,
                    'security': '8f47919783'
                };

                $.post(ajaxurl, data, function(response) {
                    $('.my-posts').append(response);
                    page++;
                });
            });
        });
    });
})(jQuery, this);




