(function ($, root, undefined) {
    $(function () {
        $(".three").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            mobileFirst: true,
            prevArrow: $(".pp2"),
            nextArrow: $(".nn2"),
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow:1
                }
            }]
        });

        $(".slick-carousel11").slick({


            cssEase: 'linear',
            nextArrow: '<i class="fa fa-angle-right next1"></i>',
            prevArrow: '<i class="fa fa-angle-left prev1"></i>',
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow:1
                }
            },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow:2
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow:3
                    }
                }
            ]
        });

        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });


        $('.slider_content').slick({
            dots: true,
            infinite: true,
            arrov:true,
            nextArrow: '<i class="fa fa-angle-right next"></i>',
            prevArrow: '<i class="fa fa-angle-left prev"></i>',
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        $('.three1').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            mobileFirst: true,
            prevArrow: $(".pp2"),
            nextArrow: $(".nn2"),
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow: 2
                }
            }]

        });





        //  +/- кол-во товаров
        jQuery(document).ready(function($){

            $(document).on('click', '.myplus', function(e) { // replace '.quantity' with document (without single quote)
                $input = $(this).prev('input.qty');
                var val = parseInt($input.val());
                var step = $input.attr('step');
                step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
                $input.val( val + step ).change();
            });
            $(document).on('click', '.myminus',  // replace '.quantity' with document (without single quote)
                function(e) {
                    $input = $(this).next('input.qty');
                    var val = parseInt($input.val());
                    var step = $input.attr('step');
                    step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
                    if (val > 0) {
                        $input.val( val - step ).change();
                    }
                });
        });

    });
})(jQuery, this);
jQuery(document).ready(function(){
// меняем цвет

    $('.select-change').click(function(ev){
        $('#pa_color').val($(this).data('val'));
        $('#pa_color').trigger('change');
        $('.variations_form').trigger('check_variations');
    });

    // $( "#pa_color" ).on( "change", function (ev) {
    //     console.log($('.variations_form'));
    //
    //
    //
    // } );


// меняем размер
    $('.size-change').click(function(ev){
        $('#pa_size').val($(this).data('val'));
        $('#pa_size').trigger('change');
        $('.variations_form').trigger('check_variations');


    });




// подсветка выбраного размера
    $('.select-change').on('click', function(ev){

        if( $('.select-change').hasClass('active-variation') && !$(this).is('active-variation') ){
            $('.active-variation').toggleClass('active-variation');
            $(this).toggleClass('active-variation');
        }
        else if( !$(this).is('active-variation') ){
            $(this).toggleClass('active-variation');
        }

    });
// подсветка выбратного цвета
    $('.size-change').on('click', function(ev){

        if( $('.size-change').hasClass('active-variation') && !$(this).is('active-variation') ){
            $('.active-variation').toggleClass('active-variation');
            $(this).toggleClass('active-variation');
        }
        else if( !$(this).is('active-variation') ){
            $(this).toggleClass('active-variation');
        }

    });



});

