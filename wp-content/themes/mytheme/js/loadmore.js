(function ($, root, undefined) {
    $(function () {
        var ajaxurl = myajax.url;
        var page = 1;
        var offset = 0;
        jQuery(function($) {
            $('body').on('click', '.loadmore', function() {

                var data = {
                    'action': 'load_posts_by_ajax',
                    'page': page,
                    'security': '8f47919783'
                };

                $.post(ajaxurl, data, function(response) {
                    if (response.replace(/ /g,'') == 'noposts')
                    { console.log ($('.loadmore'));
                        $('.loadmore').hide();
                    }else {
                    $('.my-posts').append(response);

                    page++;
                    }
                });
            });
        });
    });

})(jQuery, this);
