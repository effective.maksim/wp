jQuery(function($){
    var ajaxurl = myajax.url;
    // load more button click event
    $('.comment_loadmore').click( function(){
        var button = $(this);

        // decrease the current comment page value
        cpage--;

        $.ajax({
            url : ajaxurl, // AJAX handler, declared before
            data : {
                'action': 'cloadmore', // the parameter for admin-ajax.php
                'post_id': parent_post_id, // the current post
                'cpage' : cpage, // current comment page
            },
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.text('Loading...'); // some type of preloader
            },
            success : function( data ){
                if( data ) {
                    $('ol.comment-list').append( data ); // insert comments
                    button.text('More comments');
                    if ( cpage == 1 ) // if the last page, remove the button
                        button.remove();
                } else {
                    button.remove();
                }
            }
        });
        return false;
    });

});

