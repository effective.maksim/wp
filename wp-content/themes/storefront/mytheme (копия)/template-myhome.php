<?php
/*
Template Name: myhomepage
*/
get_header(); ?>

<!-- slider main block -->
    <div class="top-block">
        <div class="container">

    <div class='col-12 header-slides'>
        <h1 class='text-center section-title'></h1>
        <div class="slider header_slider">
            <?php
            global $redux_demo;

            if ( isset($redux_demo['opt-slides']) && !empty($redux_demo['opt-slides']) )
                foreach( $redux_demo['opt-slides'] as $key_slide):
                    ?>
                    <div class="item-slide">

                        <img src="<?php echo $key_slide['image']; ?>" alt="Slide image" />
                        <div class='mywrap'>
                            <h2 class="title-slide"><?php echo $key_slide['title']; ?></h2>
                            <a href="<?php echo $key_slide['url']; ?>" class="btn btn-primary maksbut button_slider">View:<?php echo $key_slide['title']; ?> </a>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
    </div> </div></div>



    <body>
<!-- servicess block -->
<div class="services-block">
    <div class="container">
<div class="row flexx">
            <div class="col-md-3 flex item-1">
                <span class="section-icon"><img src="http://localhost/wp/wp-content/uploads/2018/09/1.png" alt=""></span>
                <span class="section-title"><?php echo $redux_demo['section-1'];?></span>
                <p class="grey"><?php echo $redux_demo['section-1-sub'];?></p>
                            </div>
            <div class="col-md-3 flex">
                <span class="section-icon"><img src="http://localhost/wp/wp-content/uploads/2018/09/2.png" alt=""></span>
                <span class="section-title"><?php echo $redux_demo['section-2'];?></span>
                <p class="grey"><?php echo $redux_demo['section-2-sub'];?></p>
            </div>
            <div class="col-md-3 flex">
                <span class="section-icon"><img src="http://localhost/wp/wp-content/uploads/2018/09/3.png" alt=""></span>
                <span class="section-title"><?php echo $redux_demo['section-3'];?></span>
                <p class="grey"><?php echo $redux_demo['section-3-sub'];?></p>
            </div>
            <div class="col-md-3 item-4 flex">
                <span class="section-icon"><img src="http://localhost/wp/wp-content/uploads/2018/09/4.png" alt=""></span>
                <span class="section-title"><?php echo $redux_demo['section-4'];?></span>
                <p class="grey"><?php echo $redux_demo['section-4-sub'];?></p>
            </div>
</div>
    </div></div>

<!-- content block -->
<div class="container">
    <?php
    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'paged' => 1,
    );
    $query = new WP_Query( $args );
    if ( $query->have_posts() ) :
        ?>
        <div class="my-posts">
            <?php $count = '0';?>
            <?php while ( $query->have_posts() ) : $query->the_post() ?>
                <div class="col-md-3">
                    <div class="mypost">
                        <div class="borderblock">
                            <div class="post-img"><a href="<?php the_permalink(); ?>"> <?php if(has_post_thumbnail()){ the_post_thumbnail();}?></a></div>
                            <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

<!-- if isset sale price -> change class -->
                   <div class="price-color">
                       <?php
                        global $product;
                        $sale_price=$product->get_sale_price();
                        if ($sale_price !=='')
                        {
                        echo '<span class="old-price">';
                        }
                        ?>

                       <?php
                       echo $old_price=$product->get_regular_price() .' USD';
                       ?>

                       <?php
                       if ($sale_price !=='')
                       {
                       echo '</span>';
                       }
                       ?>

                    <?php
                       if ($sale_price !=='')
                       {
                           echo ' ' .$sale_price .' USD';
                       }
                       ?>
                   </div>
<!-- count pruduct -->
                    <?php
                    $count++;
                    wp_reset_postdata();
                    ?>

                    </div></div>
                </div>
<!-- Show banner after 5 count -->
                <?php  if ($count === 5 AND $redux_demo['banner-switch'] == 1) {?>
                    <div class="col-md-9 banner" >

                            <div class="img-banner-1"><a href="<?php echo $redux_demo['banner-link'];?>"><img src="<?php echo $redux_demo['banner-img']['url'];?>" alt="banner" border="0" class="banner_100" "></a></div>
                            <div class="new_button1">new</div>
                            <div class="banner-category"><?php echo $redux_demo['catalog-banner-text'];?></div>
                            <div class="title-banner12"><?php echo $redux_demo['banner-text'];?></div>
                            <div class="price-banner1">$<?php echo $redux_demo['price-banner-text'];?> USD</div>



                    </div>
                <?php }?>
<!-- END Show banner after 5 post -->
            <?php endwhile ?>
        </div>
    <?php endif ?>
</div>
<!-- END content block -->


<!-- loadmore button -->
<?php
    echo '<div class="loadmore load"><i class="fa fa-refresh"></i> Load More</div>'; // you can use <a> as well
?>
<!-- END loadmore button -->
</div>
<!-- END content block -->

<!-- After content slider -->
    <div class="container">
<div class='col-12 main-slides'>


    <div class="slider_content">
                <?php
        global $redux_demo;
        if ( isset($redux_demo['opt-slides-content']) && !empty($redux_demo['opt-slides-content']) )
            foreach( $redux_demo['opt-slides-content'] as $key_slide):
                ?>
                <div class="item-slide">

                    <a href="<?php echo $key_slide['url']; ?>"> <img class="slide_img1" src="<?php echo $key_slide['image']; ?>"  alt="Slide image" height="355"></a>

                    <div class='mywrap1'>
                       <?php echo $key_slide['description']; ?>
                    </div>

                </div>

            <?php endforeach; ?>
    </div>
</div></div>
<!-- END After content slider -->

<!-- 2 banners block -->
<div class="container">
    <!-- 1 banner block -->
        <?php  if ($redux_demo['after-banner-switch-1'] == 1) {?>

            <div class="col-md-6 banner1" >


                <div class="example2">
                    <a href="<?php echo $redux_demo['banner1-text-4'];?>"><img src="<?php echo $redux_demo['banner1-img']['url'];?>" alt="" /></a>
                    <span><div class="bannertext1"><?php echo $redux_demo['banner1-text-1'];?></div>
            <div class="bannertext2"><?php echo $redux_demo['banner1-text-2'];?></div>
            <div class="bannertext3">• <?php echo $redux_demo['banner1-text-3'];?> USD</div></span>


                </div>
            </div>
        <?php }?>
    <!-- END 1 banner block -->

<!-- banner 2 block -->
        <?php  if ($redux_demo['after-banner-switch-2'] == 1) {?>
        <div class="col-md-6 banner2" >
            <div class="example2">
                <a href="<?php echo $redux_demo['banner2-text-4'];?>"><img src="<?php echo $redux_demo['banner2-img']['url'];?>" alt="" /></a>
                <span>
                <div class="bannertext1"><?php echo $redux_demo['banner2-text-1'];?></div>
            <div class="bannertext2"><?php echo $redux_demo['banner2-text-2'];?></div>
            <div class="bannertext3">• <?php echo $redux_demo['banner2-text-3'];?> USD</div></span>
            </div>
        </div>
        <?php }?>
<!-- END banner 2 block -->
        </div>
<!-- END 2 banners block -->

<!-- Popular block -->
<!-- Popular block -->



<div class="container popular">

    <div class="col-md-3">
        <div class="banner-left">
            <div class="bannerimg">
            <a href=""><img src="<?php echo $redux_demo['banner-left-img1']['url'];?>" alt="banner" border="0"></a>
            <span class="banner-left-text"><?php echo $redux_demo['banner-left-text'];?></span>
            <span class="banner-left-count"><?php echo $redux_demo['banner-left-count'];?></span>
            <span class="banner-left-content"><?php echo $redux_demo['banner-left-content'];?></span>
            <span class="banner-left-content1"><img src="http://localhost/wp/wp-content/uploads/2018/09/back-but-1-1.gif" alt=""></span>
            <span class="banner-left-link"><a href="<?php echo $redux_demo['banner-left-link'];?>">Read more</a></span>
            </div>
               </div>



        </div>
          <div class="col-md-9 out">
        <div class="button_slide">
            <div class="popular-button">
                <div class="popular-text">POPULAR</div>
                <div class="left-button">
                    <button class="pp2"><i class="fa fa-angle-right"></i></button>
                    <button class="nn2"><i class="fa fa-angle-left"></i></button>
                </div>
            </div>



        </div>
<div class="popblock">
        <section class="three">
            <?php $mycount='0'?>
            <?php
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 6,
                'orderby' => 'title',
                'order' => 'ASC',
            );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
//цикл
                if ($mycount === '0' OR $mycount === 3 )
                {
                    echo '<div class="slider-block-all">';} // общий слайд для 3 постов
 if ($mycount === '0' OR $mycount === 3 )        //вывод 1 и 3 новости
                {?>
            <div class="slider-block1">
                <div class="slider1-img">
                    <img src="<?php the_post_thumbnail_url( array(580, 320) );;?>" alt="">
                    <div class="slider1-title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title()?></a></div> <!--если 0 открываем -->
                    <div class="slider1-date"><i class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></div> <!--если 0 открываем -->
                    <div class="slider1-com"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '% comm'); ?></div> <!--если 0 открываем -->
                    <div class="slider1-like"><i class="fa fa-heart"></i> 35</div> <!--если 0 открываем -->
                </div></div>
            <?php }?>
<?php if ($mycount === 1 OR $mycount === 3 ){?> <!--вывод 2 блока 1 новость// открываем общий див -->
            <div class="slider-block2">
                <div class="slider2-block">
                    <div class="slider2-img1"><a href="<?php echo get_permalink(); ?>"><img src="<?php the_post_thumbnail_url( array(100, 100) );;?>" alt=""></a></div>
                    <div class="slider2-date"><i class="fa fa-clock-o"></i> Date: <?php echo get_the_date(); ?></div>
                    <div class="slider2-title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title()?></a></div>
                    <div class="slider2-com"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '%'); ?></div>
                    <div class="slider2-like"><i class="fa fa-heart"></i> 35</div>
                </div>
                <?php }?>
                <?php if ($mycount === 2 OR $mycount === 5 ){?> <!--вывод 2 блока 2 новость// закрываем див -->
                <div class="slider2-block">
                        <div class="slider2-img1"><a href="<?php echo get_permalink(); ?>"><img src="<?php the_post_thumbnail_url( array(100, 100) );;?>" alt=""></a></div>
                        <div class="slider2-date"><i class="fa fa-clock-o"></i> Date: <?php echo get_the_date(); ?></div>
                        <div class="slider2-title"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title()?></a></div>
                        <div class="slider2-com"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '%'); ?></div>
                        <div class="slider2-like"><i class="fa fa-heart"></i> 35</div>
                    </div>

                    </div>
                    <div class="view-all"><a href="">VIEW ALL <i class="fa fa-arrow-right"></i></a></div>
                    <?php }?>

<?php
                if ($mycount === 2 OR $mycount === 5) // закрываем общий слайд
                {
                    echo '</div>';}
                $mycount++;
            endwhile;
            wp_reset_postdata();
            ?>
        </section>
</div>
    </div></div>

<div class="container">
    <div class="caruosel-bloc">
        <section class="slick-carousel11">
            <?php
            global $redux_demo;

            if ( isset($redux_demo['partner-slides-content']) && !empty($redux_demo['partner-slides-content']) )
            foreach( $redux_demo['partner-slides-content'] as $key_slide):
            ?>
            <div><a href="<?php echo $key_slide['url']; ?>"><img src="<?php echo $key_slide['image']; ?>" alt=""></a></div>


            <?php endforeach; ?>
        </section>
    </div>

</div>









<?php
get_footer();?>

