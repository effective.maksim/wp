<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <div class="title-pruduct">
        <div class="cat-title">
            <?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
        </div>
        <div class="breadcrump">
            <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
        </div>
    </div>
    <div class="container single_product">
<div class="product-all">

            <div class="col-md-6">
                <!--category-->
<div class="lifestyle">Lifestyle</div>
                <?php //wc_get_template_part( 'content', 'single-product' ); ?>

                <!--Title-->
                <?php the_title( '<div class="title-product">', '</div>' )?>
                <!--Rating-->
                <?php if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) {
                    return;
                }

                $rating_count = $product->get_rating_count();
                $review_count = $product->get_review_count();
                $average      = $product->get_average_rating();

                if ( $rating_count > 0 ) : ?>

                    <div class="woocommerce-product-rating">
                        <div class="rating_product">
                        <?php echo wc_get_rating_html( $average, $rating_count ); ?>
                        <?php if ( comments_open() ) : ?><a href="#Review" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( ' %s customer review', ' %s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)</a><?php endif ?>
                        </div>
                        </div>

                <?php endif; ?>

                <!--Atributes-->

                <table width="100%"><tbody><tr><td><?php echo ($product->get_attribute( 'color' )); ?></td><td class="product_table"><?php echo ($product->get_attribute( 'size' )); ?></td></tr></tbody></table>


                <!--Price-->
                <div class="price-color">
                    <?php
                    global $product;
                    $sale_price=$product->get_sale_price();
                    if ($sale_price !=='')
                    {
                        echo '<span class="old-price">';
                    }
                    ?>

                    <?php
                    echo $old_price=$product->get_regular_price() .' USD';
                    ?>

                    <?php
                    if ($sale_price !=='')
                    {
                        echo '</span>';
                    }
                    ?>

                    <?php
                    if ($sale_price !=='')
                    {
                        echo ' ' .$sale_price .' USD';
                    }
                    ?>
                </div>
                <!--add to cart -->
                <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                    <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

                    <?php
                    do_action( 'woocommerce_before_add_to_cart_quantity' );

                    woocommerce_quantity_input( array(
                        'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                        'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                        'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                    ) );

                    do_action( 'woocommerce_after_add_to_cart_quantity' );
                    ?>

                    <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                </form>

            </div>

            <div class="col-md-6">
                <?php

                global $product;

                $columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 2 );
                $post_thumbnail_id = $product->get_image_id();
                $wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
                    'woocommerce-product-gallery',
                    'woocommerce-product-gallery--' . ( has_post_thumbnail() ? 'with-images' : 'without-images' ),
                    'woocommerce-product-gallery--columns-' . absint( $columns ),
                    'images',
                ) );
                ?>

                <div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
                    <figure class="woocommerce-product-gallery__wrapper">
                        <?php
                        if ( has_post_thumbnail() ) {
                            $html  = wc_get_gallery_image_html( $post_thumbnail_id, true );
                        } else {
                            $html  = '<div class="woocommerce-product-gallery__image--placeholder">';
                            $html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
                            $html .= '</div>';
                        }

                        echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

                        do_action( 'woocommerce_product_thumbnails' );
                        ?>
                    </figure>
                </div>
            </div>


         <?php //wc_get_template_part( 'content', 'single-product' ); ?>




        <?php endwhile; // end of the loop. ?>


    </div>

    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#Review">Review</a></li>
            <li><a data-toggle="tab" href="#Specification">Specification</a></li>
            <li><a data-toggle="tab" href="#Description">Description</a></li>

        </ul>
        <!--review-->
        <div class="tab-content">
            <?php comments_template(); ?>



            </div>

            <!--Specification-->
            <div id="Specification" class="tab-pane fade">
                <?php do_action( 'woocommerce_product_additional_information', $product ); ?>
            </div>

            <!--Description-->
            <div id="Description" class="tab-pane fade">
                <?php the_content(); ?>
            </div>

        </div>
    </div>
    </div>

    <!-- content block -->
    <div class="container">
        <?php
        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'paged' => 1,
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) :
            ?>
            <div class="my-posts">
                <?php $count = '0';?>
                <?php while ( $query->have_posts() ) : $query->the_post() ?>
                    <div class="col-md-3">
                        <div class="mypost">
                            <div class="borderblock">
                                <div class="post-img"><a href="<?php the_permalink(); ?>"> <?php if(has_post_thumbnail()){ the_post_thumbnail();}?></a></div>
                                <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

                                <!-- if isset sale price -> change class -->
                                <div class="price-color">
                                    <?php
                                    global $product;
                                    $sale_price=$product->get_sale_price();
                                    if ($sale_price !=='')
                                    {
                                        echo '<span class="old-price">';
                                    }
                                    ?>

                                    <?php
                                    echo $old_price=$product->get_regular_price() .' USD';
                                    ?>

                                    <?php
                                    if ($sale_price !=='')
                                    {
                                        echo '</span>';
                                    }
                                    ?>

                                    <?php
                                    if ($sale_price !=='')
                                    {
                                        echo ' ' .$sale_price .' USD';
                                    }
                                    ?>
                                </div>
                                <!-- count pruduct -->
                                <?php
                                $count++;
                                wp_reset_postdata();
                                ?>

                            </div></div>
                    </div>
                    <!-- Show banner after 5 count -->
                    <?php  if ($count === 4){
                        break;
                    }?>
                    <!-- END Show banner after 5 post -->
                <?php endwhile ?>
            </div>
        <?php endif ?>
    </div>
    <!-- END content block -->



    <div class="container">
        <div class="caruosel-bloc">
            <section class="slick-carousel11">
                <?php
                global $redux_demo;

                if ( isset($redux_demo['partner-slides-content']) && !empty($redux_demo['partner-slides-content']) )
                    foreach( $redux_demo['partner-slides-content'] as $key_slide):
                        ?>
                        <div><a href="<?php echo $key_slide['url']; ?>"><img src="<?php echo $key_slide['image']; ?>" alt=""></a></div>


                    <?php endforeach; ?>
            </section>
        </div>

    </div>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
