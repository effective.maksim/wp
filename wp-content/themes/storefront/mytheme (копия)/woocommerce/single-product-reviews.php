<div id="Review" class="tab-pane fade in active">
    <div id="comments" class="comments-area">


        <?php

        //return $wp_query->have_comments();
        if ( have_comments() ) :
            ?>

            <h2 class="comments-title">
                <?php

                $comments_number = get_comments_number();
                if ( '1' === $comments_number ) {
                    /* translators: %s: post title */
                    printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'mytheme' ), get_the_title() );
                } else {
                    printf(
                    /* translators: 1: number of comments, 2: post title */
                        _nx(
                            '%1$s Reply to &ldquo;%2$s&rdquo;',
                            '%1$s Replies to &ldquo;%2$s&rdquo;',
                            $comments_number,
                            'comments title',
                            'mytheme'
                        ),
                        number_format_i18n( $comments_number ),
                        get_the_title()
                    );
                }
                ?>
            </h2>

            <ol class="comment-list">
                <?php
                wp_list_comments( array(
                    'avatar_size' => 100,
                    'style'       => 'ol',
                    'short_ping'  => true,

                ) );
                ?>
            </ol>
            <?php
            $cpage = get_query_var('cpage') ? get_query_var('cpage') : 1;
            if( $cpage > 1 ) {
                echo '<div class="comment_loadmore">More comments</div>
	<script>
	    parent_post_id = ' . get_the_ID() . ',
    	cpage = ' . $cpage . '
	</script>';
            }
        endif; // Check for have_comments().

        if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
            <p class="no-comments"><?php _e( 'Comments are closed.', 'mytheme' ); ?></p>
        <?php
        endif;
        comment_form();
        ?>
    </div>