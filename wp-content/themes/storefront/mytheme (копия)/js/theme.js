(function ($, root, undefined) {
    $(function () {
        $(".three").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            mobileFirst: true,
            prevArrow: $(".pp2"),
            nextArrow: $(".nn2"),
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow:1
                }
            }]
        });

        $(".slick-carousel11").slick({


            cssEase: 'linear',
            nextArrow: '<i class="fa fa-angle-right next1"></i>',
            prevArrow: '<i class="fa fa-angle-left prev1"></i>',
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow:1
                }
            },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow:2
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow:3
                    }
                }
            ]
        });

        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });


        $('.slider_content').slick({
            dots: true,
            infinite: true,
            arrov:true,
            nextArrow: '<i class="fa fa-angle-right next"></i>',
            prevArrow: '<i class="fa fa-angle-left prev"></i>',
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        $('.three1').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            mobileFirst: true,
            prevArrow: $(".pp2"),
            nextArrow: $(".nn2"),
            responsive: [{
                breakpoint: 500,
                settings: {
                    slidesToShow: 2
                }
            }]

        });


    });
})(jQuery, this);

