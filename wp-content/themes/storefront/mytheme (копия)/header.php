<!DOCTYPE html>
<html><head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri() ?>" />
    <?php wp_head() ?>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<div class="top-block">
    <div class="container">
        <header>
            <!-- logo section -->
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><?php
                            global $redux_demo;
                            echo $redux_demo['logo-text'];
                            ?></a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
                            <?php
                            $params = array(
                                'container'=> false, // Без div обертки
                                'echo'=> false, // Чтобы можно было его предварительно вернуть
                                'items_wrap'=> '%3$s', // Разделитель элементов
                                'depth'=> 0, // Глубина вложенности
                                'menu' => 'top',
                            );
                            // Чистим все теги, кроме ссылок
                            print strip_tags(wp_nav_menu( $params ), '<a>, <li>' );
                            ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                       <li><a href="<?php echo wp_login_url(); ?>"><span class="glyphicon glyphicon-log-in"></span> LOGIN</a></li>
                            <li><a href="<?php echo wp_login_url(); ?>"><span class="glyphicon glyphicon-user"></span> SIGN UP</a></li>
                            <li><?php
                            global $woocommerce; ?>

                                <?php
                                global $woocommerce; ?>
                                <a href="<?php echo wc_get_cart_url();?>" class="cart-button"><img src="http://localhost/wp/wp-content/uploads/2018/09/Untitled-21.gif" alt=""></a>
                                <span class="button_count"><?php echo sprintf($woocommerce->cart->cart_contents_count); ?></span>
                            </li>

                           
                            <li><img src="http://localhost/wp/wp-content/uploads/2018/09/searchbut.gif" alt="" class="searchbut"></li>

                        </ul>
                    </div>
                </div>
            </nav>
</div>

            <!-- END menu -->

            <!-- Cart link -->

            <!-- END Cart link -->

            <!-- Login link -->

            <!--END  Login link -->
                    </header>
    </div>
</div>




