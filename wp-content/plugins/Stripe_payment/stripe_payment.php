<?php
/*
 * Plugin Name:Stripe Payment Gateway

 * Description: Take credit card payments on your store.
 * Author: Maksim Akhmetov
 * Version: 1.0.1
 */
function stripe_add_your_gateway_class( $methods ) {
    $methods[] = 'WC_Stripe_Gateway';
    return $methods;
}

add_filter( 'woocommerce_payment_gateways', 'stripe_add_your_gateway_class' );
add_action( 'plugins_loaded', 'stripe_init_gateway_class' );

function stripe_init_gateway_class()
{
    class WC_Stripe_Gateway extends WC_Payment_Gateway
    {
        public function __construct() {

            $this->id = 'stripe'; //the unique ID for this gateway
            $this->icon = ''; //logo
            $this->has_fields = true;
            $this->method_title = 'Stripe'; //show in admin
            $this->method_description = 'New paymant method'; //desc in admin

            // Method with all the options fields
            $this->init_form_fields();

            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            $this->private_key = $this->testmode ? $this->get_option( 'test_private_key' ) : $this->get_option( 'private_key' );
            $this->publishable_key = $this->testmode ? $this->get_option( 'test_publishable_key' ) : $this->get_option( 'publishable_key' );

            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
             }

        /**
         * Initialize Gateway Settings Form Fields
         */
        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => 'Enable/Disable',
                    'label' => 'Enable Strie Gateway',
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'title' => array(
                    'title' => 'Title',
                    'type' => 'text',
                    'description' => 'Title',
                    'default' => 'Credit Card',
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => 'Description',
                    'type' => 'textarea',
                    'description' => 'Description 1.',
                    'default' => 'Pay 11De11scription.',
                ),
                'testmode' => array(
                    'title' => 'Test mode',
                    'label' => 'Enable Test Mode',
                    'type' => 'checkbox',
                    'description' => 'Test API keys.',
                    'default' => 'yes',
                    'desc_tip' => true,
                ),
                'test_publishable_key' => array(
                    'title' => 'Test Publishable Key',
                    'type' => 'text'
                ),
                'test_private_key' => array(
                    'title' => 'Test Private Key',
                    'type' => 'password',
                ),
                'publishable_key' => array(
                    'title' => 'Live Publishable Key',
                    'type' => 'text'
                ),
                'private_key' => array(
                    'title' => 'Live Private Key',
                    'type' => 'password'
                )
            );
        }

        //handling payment and processing the order
        function process_payment( $order_id ) {
            global $woocommerce;
            $order = new WC_Order( $order_id );
            // Mark as on-hold (we're awaiting the cheque)
            $order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));
            // Reduce stock levels
            $order->reduce_order_stock();
            // Remove cart
            $woocommerce->cart->empty_cart();
            // Return thankyou redirect
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
            //if succes
            $order->payment_complete();
        }

        public function payment_fields() {
            echo 'form action with fields';

        }

        public function validate_fields() {
// Return true if the form passes validation or false if it fails. You can use the wc_add_notice() function if you want to add an error and display it to the user.
        }

        public function payment_scripts() {

         }

        /*
         * In case you need a webhook, like PayPal IPN etc
         */
        public function webhook() {

           }


    }
}